# README

## Synopsis

This project should be Rails on Steroids.
It makes it easier to apply to the methodology of a [Twelve-Factor App](http://12factor.net/),
enforces a [well defined style guide](https://github.com/bbatsov/ruby-style-guide), should be
[test friendly](https://github.com/rspec/rspec), help with
[clean code](https://github.com/joenas/preek) and
[improve security](https://github.com/presidentbeef/brakeman).

## Important Notes

* This project has an file under /config named monkeypatches, which already says what it does.
  Please put all core extensions into this file.
* There are no Rails environments, add configuration in application.rb and change it via environment
  variables.

## Usage

* Fork
* Duplicate application_example.yml as application.yml and change ENV as you need
* Setup environment via `bundle && rake db:create && bundle exec guard`
* Start Rails server `rails s`
