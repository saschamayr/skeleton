guard :rspec, cmd: 'bundle exec rspec' do
  watch(%r{^spec/.+_spec\.rb$})
  watch(%r{^lib/(.+)\.rb$})     { |m| "spec/lib/#{m[1]}_spec.rb" }
  watch('spec/spec_helper.rb')  { 'spec' }
  watch(%r{^app/(.+)\.rb$})                           { |m| "spec/#{m[1]}_spec.rb" }
  watch(%r{^app/controllers/(.+)_(controller)\.rb$})  do |m|
    %W(spec/routing/#{m[1]}_routing_spec.rb
       spec/#{m[2]}s/#{m[1]}_#{m[2]}_spec.rb
       spec/acceptance/#{m[1]}_spec.rb)
  end
  watch(%r{^spec/support/(.+)\.rb$})                  { 'spec' }
  watch('config/routes.rb')                           { 'spec/routing' }
  watch('app/controllers/application_controller.rb')  { 'spec/controllers' }
  watch('spec/rails_helper.rb')                       { 'spec' }
end

guard :rubocop, cli: %w(--format clang --rails --display-cop-names) do
  watch(/.+\.rb$/)
  watch(%r{(?:.+\/)?\.rubocop\.yml$}) { |m| File.dirname(m[0]) }
end

guard :bundler do
  watch('Gemfile')
end

guard 'brakeman', run_on_start: true, quiet: true do
  watch(%r{^app/.+\.rb$})
  watch(%r{^config/.+\.rb$})
  watch(%r{^lib/.+\.rb$})
  watch('Gemfile')
end

guard :preek, run_all_dir: 'app', cli: %w(--compact) do
  watch(%r{^app/.+\.rb$})
end
