require File.expand_path('../boot', __FILE__)

# Pick the frameworks you want:
require 'active_model/railtie'
require 'active_record/railtie'
require 'action_controller/railtie'
require 'action_mailer/railtie'
# require 'action_view/railtie'
# require 'sprockets/railtie'
# require 'rails/test_unit/railtie'

# Require the gems listed in Gemfile, including any gems
# you've limited to :test, :development, or :production.
Bundler.require(*Rails.groups)

module Skeleton
  class Application < Rails::Application
    # Annotate each configuration which is not nil or false by default with its default value,
    # so that you can decide which ENVs are important to set

    config.cache_classes = ENV['RAILS_CACHE_CLASSES'].try(:to_b)
    config.eager_load = ENV['RAILS_EAGER_LOAD'].try(:to_b)
    config.consider_all_requests_local = ENV['RAILS_CONSIDER_ALL_REQUESTS_LOCAL'].try(:to_b)
    config.action_controller.perform_caching = ENV['RAILS_PERFORM_CACHING'].try(:to_b)
    config.action_mailer.raise_delivery_errors = ENV['RAILS_RAISE_MAIL_DELIVERY_ERRORS'].try(:to_b)
    config.active_support.deprecation = ENV['RAILS_DEPRECATION_HANDLING'].try(:to_sym)
    config.active_record.migration_error = ENV['RAILS_MIGRATION_ERROR'].try(:to_sym)
    config.action_view.raise_on_missing_translations = ENV['RAILS_TRANSLATIONS_ERROR'].try(:to_b)
    # Defaults to true
    config.action_dispatch.show_exceptions = ENV['RAILS_SHOW_EXCEPTIONS'].try(:to_b)
    config.action_controller.allow_forgery_protection = ENV['RAILS_PROTECT_FROM_FORGERY'].try(:to_b)
    config.action_mailer.delivery_method = ENV['RAILS_PROTECT_FROM_FORGERY'].try(:to_sym)
    # Defaults to true
    config.serve_static_assets = ENV['RAILS_SERVE_STATIC_ASSETS'].try(:to_b)
    # Defaults to :debug
    config.log_level = ENV['RAILS_LOG_LEVEL'].try(:to_sym)
    # Defaults to {}
    config.i18n.fallbacks = ENV['RAILS_I18N_FALLBACKS'].try(:to_b)

    # Default active configuration in production environment
    # Please delete this lines after setting them in ENV on a production server

    # config.cache_classes = true
    # config.eager_load = true
    # config.action_controller.perform_caching = true
    # config.active_support.deprecation = :notify
    # config.serve_static_assets = false
    # config.log_level = :info
    # config.i18n.fallbacks = true
  end
end
