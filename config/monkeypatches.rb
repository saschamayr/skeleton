# Useful for converting boolean params from external services and from environment variables
class String
  def to_b
    self == 'true'
  end
end
